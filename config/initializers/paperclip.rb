Spree::Image.attachment_definitions[:attachment][:styles] = {
  mini: '60x60>',
  small: '107.17x107.17>',
  product: '459x459>',
  large: '800x800>'
}
