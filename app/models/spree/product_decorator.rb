Spree::Product.class_eval do
  scope :related_products_of, ->(product) {
    in_taxons(product.taxons).
      with_images_and_prices.
      without_self(product).
      distinct.
      order(Arel.sql('RAND()'))
  }
  scope :without_self, ->(product) { where.not(id: product.id) }
  scope :with_images_and_prices, -> { includes(master: [:images, :default_price]) }
end
