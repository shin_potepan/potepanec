require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:tshirt) { create(:taxon, name: 'Tshirt') }
    let(:bag) { create(:taxon, name: 'bag') }
    let!(:product1) { create(:product, taxons: [tshirt]) }
    let!(:product2) { create(:product, taxons: [tshirt]) }
    let!(:product3) { create(:product, taxons: [bag]) }

    before do
      get :show, params: { id: product1.id }
    end

    it "response successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it "renders show template" do
      expect(response).to render_template(:show)
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product1
    end

    it "assings @related_products" do
      expect(assigns(:related_products)).to match_array [product2]
      expect(assigns(:related_products)).not_to include product3
      expect(assigns(:related_products)).not_to include product1
    end

    context "There are 3 related_products in the DB" do
      let!(:product4) { create(:product, taxons: [tshirt]) }
      let!(:product5) { create(:product, taxons: [tshirt]) }

      it "assigns 3 @related_products" do
        expect(assigns(:related_products)).to match_array [product2, product4, product5]
        expect(assigns(:related_products).size).to eq 3
      end
    end

    context "There are 5 related_products in the DB" do
      let!(:product4) { create(:product, taxons: [tshirt]) }
      let!(:product5) { create(:product, taxons: [tshirt]) }
      let!(:product6) { create(:product, taxons: [tshirt]) }

      it "assigns 4 @related_products" do
        expect(assigns(:related_products)).to match_array [product2, product4, product5, product6]
        expect(assigns(:related_products).size).to eq 4
      end
    end

    context "There are 5 related_products in the DB" do
      let!(:related_products) { create_list(:product, 4, taxons: [tshirt]) }

      it "assigns 4 @related_products" do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end
