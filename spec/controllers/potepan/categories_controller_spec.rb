require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let!(:category) { create(:taxonomy, name: 'category') }
  let!(:brand) { create(:taxonomy, name: "brand") }
  let(:clothes) do
    create(:taxon, name: 'clothes', taxonomy: category, parent: category.root)
  end
  let(:tshirt) { create(:taxon, name: 'Tshirt', taxonomy: category, parent: clothes) }
  let(:bag) { create(:taxon, name: 'bag', taxonomy: category, parent: category.root) }
  let(:product1) { create(:product, taxons: [tshirt]) }
  let(:product2) { create(:product, taxons: [tshirt]) }
  let(:product3) { create(:product, taxons: [bag]) }

  describe "#show" do
    before do
      get :show, params: { id: tshirt.id }
    end

    it "responses successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status 200
    end

    it "renders show template" do
      expect(response).to render_template(:show)
    end

    it "assigns @taxonomies " do
      expect(assigns(:taxonomies)).to match_array [category, brand]
    end

    it "assigns @taxon" do
      expect(assigns(:taxon)).to eq tshirt
    end

    it "assigns @products" do
      expect(assigns(:products)).to match_array [product1, product2]
    end
  end
end
