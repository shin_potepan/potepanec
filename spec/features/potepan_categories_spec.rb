require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:category) { create(:taxonomy, name: 'category') }
  let(:clothes) do
    create(:taxon, name: 'clothes', taxonomy: category, parent: category.root)
  end
  let(:tshirt) { create(:taxon, name: 'Tshirt', taxonomy: category, parent: clothes) }
  let(:bag) { create(:taxon, name: 'bag', taxonomy: category, parent: category.root) }
  let!(:product1) { create(:product, taxons: [tshirt]) }
  let!(:product2) { create(:product, taxons: [tshirt]) }
  let!(:product3) { create(:product, taxons: [bag]) }

  scenario "user can see a category page" do
    visit potepan_category_path(id: tshirt.id)
    expect(page).to have_title "#{tshirt.name} - BIGBAG Store"
    expect(page).to have_selector 'h2', text: tshirt.name
    click_on category.name
    aggregate_failures do
      expect(page).to have_selector 'a', text: category.name
      expect(page).to have_selector 'a', text: "#{tshirt.name}(#{tshirt.products.count})"
      expect(page).to have_selector 'a', text: "#{bag.name}(#{bag.products.count})"
      expect(page).not_to have_content clothes.name
    end

    aggregate_failures do
      expect(page).to have_selector 'h5', text: product1.name
      expect(page).to have_selector 'h3', text: product1.display_price
      expect(page).to have_selector 'h5', text: product2.name
      expect(page).to have_selector 'h3', text: product2.display_price
      expect(page).not_to have_content product3.name
    end
  end

  scenario "user can see a product page via a category page" do
    visit potepan_category_path(id: bag.id)
    find("#product-#{product3.id}").click
    expect(page).to have_current_path potepan_product_path(product3.id)
  end

  scenario "user can see another category page via a category page" do
    visit potepan_category_path(id: bag.id)
    click_on category.name
    find("#taxon-#{tshirt.id}").click
    expect(page).to have_current_path potepan_category_path(tshirt.id)
  end
end
