require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:category) { create(:taxonomy, name: 'category') }
  let(:clothes) do
    create(:taxon, name: 'clothes', taxonomy: category, parent: category.root)
  end
  let(:tshirt) { create(:taxon, name: 'Tshirt', parent: clothes) }
  let(:bag) { create(:taxon, name: 'bag') }
  let!(:product1) { create(:product, taxons: [tshirt]) }
  let!(:product2) { create(:product, taxons: [tshirt]) }
  let!(:product3) { create(:product, taxons: [bag]) }

  scenario "user can see a product page" do
    visit potepan_product_path(product1.id)
    expect(page).to have_title "#{product1.name} - BIGBAG Store"
    aggregate_failures do
      within("div#product-#{product1.id}") do
        expect(page).to have_selector "h2", text: product1.name
        expect(page).to have_selector "h3", text: product1.display_price
      end
    end
    aggregate_failures do
      within("a#related-product-#{product2.id}") do
        expect(page).to have_selector "h5", text: product2.name
        expect(page).to have_selector "h3", text: product2.display_price
      end
      expect(page).not_to have_content product3.name
      expect(page).not_to have_selector "a#related-product-#{product1.id}"
    end
  end

  scenario "user can go to a related product page via a product page" do
    visit potepan_product_path(product1.id)

    expect(page).to have_link "related-product-#{product2.id}"
    expect(page).not_to have_link "related-product-#{product3.id}"

    find("#related-product-#{product2.id}").click

    expect(page).to have_current_path potepan_product_path(product2.id)
  end

  scenario "user can go to a category page via a product page" do
    visit potepan_product_path(product1.id)
    within("div#product-#{product1.id}") do
      category_link = find('#category-link')
      expect(category_link[:href]).to eq potepan_category_path(category.root.id)
      category_link.click
    end
    expect(page).to have_title "#{category.name} - BIGBAG Store"
  end
end
